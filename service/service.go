package service

import (
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/library/db"
	"gitlab.com/library/model"
	"gitlab.com/library/response"
	"golang.org/x/crypto/bcrypt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	_ "os"
	"time"
)

func AddBook(r *http.Request) response.Response {
	bytes := []byte("test")
	f, _, err := r.FormFile("book")
	fmt.Println(f.Read(bytes))
	fmt.Println(f.Read(bytes))
	file, _ := os.Create("witcher")
	file.Write(bytes)
	if err != nil {
		log.Fatal(err)
	}
	return response.Response{ Status: http.StatusOK, Message: "Book added successfully"}
}

var mySigningKey = []byte("secret")

func getToken(user model.DevUser) response.Token {
	claims := jwt.MapClaims{
		"id": user.ID,
		"name": user.Name,
		"email": user.Email,
		"authority": user.Authority,
		"exp":  time.Now().Add(time.Hour * 24).Unix(),
	}

	token := jwt.New(jwt.SigningMethodHS256)
	token.Claims = claims

	tokenString, _ := token.SignedString(mySigningKey)
	return response.Token{Token:tokenString}
}

func SignUp(r * http.Request) response.Response{
	var devUser model.DevUser
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return response.Response{
			Status:  500,
			Message: "Internal server error",
		}
	}
	err = json.Unmarshal(b, &devUser)
	if err != nil {
		fmt.Println(err)
		return response.Response{
			Status:  500,
			Message: "Internal server error",
		}
	}
	hash, err := bcrypt.GenerateFromPassword([]byte(devUser.Password), bcrypt.MinCost)
	if err != nil {
		return response.Response{
			Status:  500,
			Message: "Internal server error, error password hash",
		}
	}
	devUser.Password = string(hash)
	err = db.Db.Save(&devUser).Error
	fmt.Println(err)
	return response.Response{Status: 200, Message: "User saved successfully"}
}

func SignIn(r *http.Request) (response.Token, error){
	var user model.DevUser
	var devUser model.DevUser
	b, err := ioutil.ReadAll(r.Body)
	err = json.Unmarshal(b, &user)
	db.Db.Where("email = ?", user.Email).Find(&devUser)
	err = bcrypt.CompareHashAndPassword([]byte(devUser.Password), []byte(user.Password))
	token := getToken(user)

	return token, err

}