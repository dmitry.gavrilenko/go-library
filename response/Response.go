package response

type Response struct {
	Status  uint
	Message string
}

type Token struct {
	Token string
}
