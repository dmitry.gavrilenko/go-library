package server

import (
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"gitlab.com/library/service"
	"go-jwt-middleware"
	"log"
	"net/http"
	"time"
)

var mySigningKey = []byte("secret")

func greetingHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello"))
}



var jwtMiddleware = jwtmiddleware.New(jwtmiddleware.Options{
	SigningMethod: jwt.SigningMethodHS256,
	ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
		return mySigningKey, nil
	},
})

var securedHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("This is secured path"))
})

var addBook = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	response := service.AddBook(r)
	b, err := json.Marshal(response)
	if err != nil {
		log.Fatal("Pizdec")
	}
	w.Write(b)
})

func signUpHandler(w http.ResponseWriter, r *http.Request) {
	b, err := json.Marshal(service.SignUp(r))
	if err != nil {
		fmt.Println("Serialize error")
	}
	w.Write(b)
}

func signInHandler(w http.ResponseWriter, r *http.Request) {
	token, err := service.SignIn(r)
	if err != nil {
		fmt.Println(err)
	}
	b, err := json.Marshal(token)
	if err != nil {
		fmt.Println("Serialize error")
	}
	w.Write(b)
}

func Init() {
	route := mux.NewRouter()

	route.HandleFunc("/greeting", greetingHandler).Methods("GET")
	route.HandleFunc("/sign-up", signUpHandler).Methods("POST")
	route.HandleFunc("/sign-in", signInHandler).Methods("POST")
	route.Handle("/secured/add-book", jwtMiddleware.Handler(addBook)).Methods("POST")
	route.Handle("/secured/greeting", jwtMiddleware.Handler(securedHandler)).Methods("GET")
	fmt.Println(time.Now().Format(time.ANSIC), ":", "Server started, listen port: 3000")
	err := http.ListenAndServe(":3000", route)
	if err != nil {
		log.Fatal("Failed initialized server", err)
	}
}
