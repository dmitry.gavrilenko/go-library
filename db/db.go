package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/library/model"
	"log"
	"time"
)

const (
	dialect  = "postgres"
	database = "host=127.0.0.1 port=5432 user=root dbname=go_library password=root"
)

var Db *gorm.DB

func Init() *gorm.DB {
	var err error
	Db, err = gorm.Open(dialect, database)
	if err != nil {
		log.Fatal("Error init database", err)
	}
	fmt.Println(time.Now().Format(time.ANSIC), ":" ,"Database was initialized")
	Db.AutoMigrate(model.DevUser{}, model.Book{}, model.Publisher{}, model.Author{})
	return Db
}
