package model

import "github.com/jinzhu/gorm"

type Book struct {
	gorm.Model
	BookPath    string `gorm:"column:book_path"`
	Title       string
	Isbn        string
	Date        string
	rate        int
	PublisherId string `gorm:"column:publisher_id"`
	AuthorId    string
	UserId      string
	Ganres      []Ganre `gorm:"many2many:book_ganres;"`
}

type DevUser struct {
	gorm.Model
	Name      string
	Email     string
	Password  string
	Authority string
	Book      Book `gorm:"foreignkey:UserId"`
}

type Publisher struct {
	gorm.Model
	Name string
	Book Book `gorm:"foreignkey:PublisherId"`
}

type Author struct {
	gorm.Model
	Name     string
	Birthday string
	Book     Book `gorm:"foreignkey:AuthorId"`
}

type Ganre struct {
	gorm.Model
	Name string
	Book []Book `gorm:"many2many:book_ganres;"`
}
