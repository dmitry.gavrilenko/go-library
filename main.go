package main

import (
	"gitlab.com/library/db"
	"gitlab.com/library/server"
)

func main() {
	db.Init()
	server.Init()
}
